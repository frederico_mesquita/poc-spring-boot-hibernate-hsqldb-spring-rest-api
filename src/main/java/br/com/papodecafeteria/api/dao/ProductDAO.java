package br.com.papodecafeteria.api.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class ProductDAO{		
	
	public Product getById(final int pId) {
		getEntityManager().flush();
		Product product = null;
		try {
			product = (Product) getEntityManager()
				.createQuery("from Product where ikey = :key")
				.setParameter("key", pId).getSingleResult();
		} catch (Exception exc) {
			Logger.getLogger(getClass().getName()).info(exc.getMessage());
		}
        return product;
	}
	
	@SuppressWarnings("unchecked")
	public List<Product> getProducts() {
		getEntityManager().flush();
    	List<Product> lstProduct = null;
    	try {
    		lstProduct = getEntityManager().createQuery("from Product").getResultList();
    	} catch (Exception exc) {
    		Logger.getLogger(getClass().getName()).info(exc.getMessage());
		}
		return lstProduct;
    }
	
    public Product save(Product pProduct) {
		try {
			getEntityManager().merge(pProduct);
		} catch (Exception ex) {
			Logger.getLogger(getClass().getName()).info(ex.getMessage());
		}
		return pProduct;
    }
    
    public void update(Product pProduct) {
    	getEntityManager().flush();
		try {
			getEntityManager().merge(pProduct);
		} catch (Exception ex) {
			Logger.getLogger(getClass().getName()).info(ex.getMessage());
		}
    }
    
    private void delete(Product pProduct) {
    	getEntityManager().flush();
		try {
			getEntityManager().remove(pProduct);
		} catch (Exception ex) {
			Logger.getLogger(getClass().getName()).info(ex.getMessage());
		}
    }
    
    public void deleteById(final int id) {
    	getEntityManager().flush();
        try {
        	Product product = getById(id);
        	delete(product);
        } catch (Exception ex) {
        	Logger.getLogger(getClass().getName()).info(ex.getMessage());
        }
    }
    
    @SuppressWarnings("unchecked")
	public List<Product> getLstProductsRelatatedByProductOrImage(int pKey) {
    	getEntityManager().flush();
    	List<Product> lstProduct = null;
    	String cQuery = "from Product p left join p.lstImages i"
    			+ "where (p.iKey = :key) or (p.parentProduct.iKey = :key) or (i.product.ikey = :key)";
    	try {
    		lstProduct = getEntityManager().createQuery(cQuery)
    			.setParameter("key", pKey)
    			.getResultList();
    	} catch (Exception exc) {
    		Logger.getLogger(getClass().getName()).info(exc.getMessage());
		}
		return lstProduct;
    }
    
    @SuppressWarnings("unchecked")
	public List<Product> getIsolatedProductByIdRroductsRelatedByProductOrImage(int pKey, int pRelatedKey){
    	getEntityManager().flush();
    	List<Product> lstProduct = null;
    	String cQuery = "from Product as p inner join p.lstImages as i inner join p.lstProducts j "
    			+ "where (p.iKey = :pk) and ((i.product.iKey = :key) or (j.parentProduct.iKey = :key))";
    	try {
    		lstProduct = getEntityManager().createQuery(cQuery)
    			.setParameter("pk", pKey)
    			.setParameter("key", pRelatedKey)
    			.getResultList();
    	} catch (Exception exc) {
    		Logger.getLogger(getClass().getName()).info(exc.getMessage());
		}
		return lstProduct;
    }

	public EntityManager getEntityManager() {
		return entityManager;
	}
	
	@PersistenceContext
	private EntityManager entityManager;

}
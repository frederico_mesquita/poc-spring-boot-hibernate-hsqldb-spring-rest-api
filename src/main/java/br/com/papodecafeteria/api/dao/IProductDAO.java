package br.com.papodecafeteria.api.dao;

import java.util.List;

interface IProductDAO {
	public Product getById(final int pId);
	
	public List<Product> getProducts();
	
	public void save(Product pProduct);
	
	public void update(Product pProduct);
	
	public void deleteById(final int id);
	
	public List<Product> getLstProductsRelatatedByProductOrImage(int pKey);
	
	public Product getIsolatedProductByRelatatedByProductOrImage(int pKey, int pRelatedKey);
	
	
	
}

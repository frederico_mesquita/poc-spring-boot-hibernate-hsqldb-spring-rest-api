package br.com.papodecafeteria.api.dao;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Image")
public class Image {
	
	/** Identificador do produto. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int	iKey;//
	/** Tipo da imagem. */
	@NotNull
	private String cType;
	/** Produto ao qual a imagem está associada. */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="productFk")
	private Product product;
	
	public Image(){}
	
	public Image(int pKey, String pType){
		setiKey(pKey);
		setcType(pType);
	}
	
	public int getiKey() {
		return iKey;
	}

	public void setiKey(int iKey) {
		this.iKey = iKey;
	}

	public String getcType() {
		return cType;
	}

	public void setcType(String cType) {
		this.cType = cType;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
	
	
}
package br.com.papodecafeteria.api.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "Product")
public class Product{
	
	/** Identificador do produto. */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int	iKey;
	/** Noome do produto. */
	@NotNull
	private String 	cName = "";
	/** Descrição do produto. */
	@NotNull
	private String 	cDescription = "";
	/** Lista de imagens associadas. */
	@OneToMany(mappedBy = "product", targetEntity = Image.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Image> lstImages;
	/** Lista de produtos associados. */
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="parentProductKey")
    private Product parentProduct;
	@OneToMany(mappedBy = "parentProduct", targetEntity = Product.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Product> lstProducts;
	
	public Product (){}
	
	public Product(int pKey, String pName, String pDescription, List<Image> pLstImages, List<Product> pLstProducts){
		if(pName.equals(null) || pDescription.equals(null)){
			Logger.getLogger(getClass().getName()).info("IllegalArgumentException");
			throw new IllegalArgumentException("IllegalArgumentException");
		}
		setiKey(pKey);
		setcName(pName);
		setcDescription(pDescription);
		setLstImages(pLstImages);
		//setLstProducts(pLstProducts);
	}

	public int getiKey() {
		return iKey;
	}

	public void setiKey(int pKey) {
		this.iKey = pKey;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcDescription() {
		return cDescription;
	}

	public void setcDescription(String cDescription) {
		this.cDescription = cDescription;
	}

	public List<Image> getLstImages() {
		return lstImages;
	}

	public void setLstImages(List<Image> lstImages) {
		this.lstImages = lstImages;
	}

	public List<Product> getLstProducts() {
		return lstProducts;
	}

	public void setLstProducts(List<Product> lstProducts) {
		this.lstProducts = lstProducts;
	}

	public Product getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}
}
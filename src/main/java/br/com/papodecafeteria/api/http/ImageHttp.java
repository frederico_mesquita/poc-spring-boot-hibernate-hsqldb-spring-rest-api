package br.com.papodecafeteria.api.http;

import java.util.logging.Logger;

import org.json.simple.JsonObject;

import br.com.papodecafeteria.api.dao.Image;
import br.com.papodecafeteria.api.integration.HttpCore;

public class ImageHttp implements HttpCore{
	protected Logger l = Logger.getLogger(getClass().getName());
	
	protected final String cIllegalArgumentException = "IllegalArgumentException";
	
	protected final String cIllegalAccessError= "IllegalAccessError";
	
	/** Identificador do produto. */
	private int	iKey;
	/** Tipo da imagem. */
	private String cType;
	
	private static ImageHttp _uniqueInstance = null;
	
	private ImageHttp(){}
	
	private ImageHttp(int pKey, String pType){
		setiKey(pKey);
		setcType(pType);
	}
	
	public static ImageHttp getInstance(){
		return new ImageHttp();
	}
	
	public static ImageHttp getInstance(int	pKey, String pType){
		return new ImageHttp(pKey, pType);
	}
	
	public static ImageHttp getNewInstance(String pType){
		set_uniqueInstance(new ImageHttp());
		_uniqueInstance.setcType(pType);
		return get_uniqueInstance();
	}
	
	public static ImageHttp getNewInstance(int	pKey, String pType){
		set_uniqueInstance(new ImageHttp(pKey, pType));
		return get_uniqueInstance();
	}
	
	public void clear() throws Exception{
		if (null == get_uniqueInstance()){
			getL().info(getcIllegalAccessError());
			throw new Exception(getcIllegalAccessError());
		}
		setiKey(0);
		setcType("");
	}
	
	public void reset() throws Exception{
		clear();
		set_uniqueInstance(null);
	}
	
	public void copy(Object pObject) throws Exception{
		ImageHttp pImageHttp = null;
		
		if(pObject.getClass().getName().equalsIgnoreCase("br.com.montreal.api.dao.ImageHttp")) {
			pImageHttp = (ImageHttp) pObject;
			clear();
			setiKey(pImageHttp.getiKey());
			setcType(pImageHttp.getcType());
		} else if(pObject.getClass().getName().equalsIgnoreCase("br.com.montreal.api.dao.Image")) {
			Image pImage = (Image) pObject;
			clear();
			setiKey(pImage.getiKey());
			setcType(pImage.getcType());
		}
	}
	
	public JsonObject export(){
		JsonObject jSONObject = new JsonObject();
		jSONObject.put("key", getiKey());
		jSONObject.put("type", getcType());
		return jSONObject;
	}
	
	public Image exportImage(){
		return (new Image(getiKey(), getcType()));
	}

	public int getiKey() {
		return iKey;
	}

	public void setiKey(int iKey) {
		this.iKey = iKey;
	}

	public String getcType() {
		return cType;
	}

	public void setcType(String cType) {
		this.cType = cType;
	}

	private static ImageHttp get_uniqueInstance() {
		return _uniqueInstance;
	}

	private static void set_uniqueInstance(ImageHttp _uniqueInstance) {
		ImageHttp._uniqueInstance = _uniqueInstance;
	}

	public Logger getL() {
		return l;
	}

	public String getcIllegalArgumentException() {
		return cIllegalArgumentException;
	}

	public String getcIllegalAccessError() {
		return cIllegalAccessError;
	}
}

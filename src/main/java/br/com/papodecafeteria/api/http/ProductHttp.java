package br.com.papodecafeteria.api.http;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.json.simple.DeserializationException;
import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import br.com.papodecafeteria.api.dao.Image;
import br.com.papodecafeteria.api.dao.Product;
import br.com.papodecafeteria.api.integration.HttpCore;

public class ProductHttp implements HttpCore{
	protected Logger l = Logger.getLogger(getClass().getName());
	
	private final String cIllegalArgumentException = "IllegalArgumentException";
	private final String cIllegalAccessError= "IllegalAccessError";
	
	/** Identificador do produto. */
	private int	iKey = 0;
	/** Noome do produto. */
	private String 	cName = "";
	/** Descrição do produto. */
	private String 	cDescription = "";
	/** Produto pai. */
	private ProductHttp	parentProductHttp;
	/** Lista de imagens associadas ao produto. */
	private List<ImageHttp> lstImages = null;
	/** Lista dos produtos filhos associados. */
	private List<ProductHttp> lstProducts = null;
	
	private static ProductHttp _uniqueInstance = null;
	
	private ProductHttp(){}
	
	private ProductHttp(JsonObject pJSONObject){
		try {
			if(Integer.parseInt(pJSONObject.get("key").toString()) != 0)
				setiKey(Integer.parseInt(pJSONObject.get("key").toString()));
		} catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		
		setcName(pJSONObject.get("name").toString());
		
		setcDescription(pJSONObject.get("description").toString());
		
		if(null != pJSONObject.get("parentProduct")){
			try {
				setParentProductHttp(new ProductHttp((JsonObject) pJSONObject.get("parentProduct")));
			} catch (Exception exc) {
				getL().info(exc.getMessage());
			}
		} else
			setParentProductHttp(null);
		
		List<JsonObject> lstJObj = new ArrayList<JsonObject>();
		List<ImageHttp> lstImageHttp = new ArrayList<ImageHttp>();
		try {
			JsonArray jArr = (JsonArray) Jsoner.deserialize((String) pJSONObject.get("images"));
			for(int a = 0; a < jArr.size(); a++)
				lstJObj.add((JsonObject) jArr.get(a));
			for(JsonObject jObj:lstJObj){
				if(jObj.getInteger("key") == 0)
					lstImageHttp.add(ImageHttp.getNewInstance(jObj.getString("type")));
				else
					lstImageHttp.add(ImageHttp.getNewInstance(jObj.getInteger("key"), jObj.getString("type")));
			}
		} catch (DeserializationException exc) {
			getL().info(exc.getMessage());
		}
		setLstImages(lstImageHttp);
		
		List<ProductHttp> lstProductHttp = new ArrayList<ProductHttp>();
		try {
			String cProducts = (String) pJSONObject.get("products");
			if((null != cProducts) && cProducts.length() > 0){
				JsonArray jArr = (JsonArray) Jsoner.deserialize(cProducts);
				for(int a = 0; a < jArr.size(); a++)
					lstProductHttp.add(new ProductHttp((JsonObject) jArr.get(a)));
			}
		} catch (DeserializationException exc) {
			getL().info(exc.getMessage());
		}
		setLstProducts(lstProductHttp);
	}
	
	public static ProductHttp getInstance(){
		return new ProductHttp();
	}
	
	public static ProductHttp getNewInstance(){
		return new ProductHttp();
	}
	
	public static ProductHttp getInstance(JsonObject pJSONObject){
		return (new ProductHttp(pJSONObject));
	}
	
	public void clear() throws Exception{
		if (null == get_uniqueInstance()){
			getL().info(getcIllegalAccessError());
			throw new Exception(getcIllegalAccessError());
		}
		setiKey(0);
		setcName("");
		setParentProductHttp(null);
		setLstImages(null);
		setLstProducts(null);
	}
	
	public void reset() throws Exception{
		clear();
		set_uniqueInstance(null);
	}
	
	public void copy(Object pObject) throws Exception{
		ProductHttp pProductHttp = null;
		
		if(pObject.getClass().getName().equalsIgnoreCase("br.com.montreal.api.dao.ProductHttp")){
			pProductHttp = (ProductHttp) pObject;

			setiKey(pProductHttp.getiKey());
			setcName(pProductHttp.getcName());
			setcDescription(pProductHttp.getcDescription());
			setParentProductHttp(pProductHttp.getParentProductHttp());
			setLstImages(pProductHttp.getLstImages());
			setLstProducts(pProductHttp.getLstProducts());
		} else if(pObject.getClass().getName().equalsIgnoreCase("br.com.montreal.api.dao.Product")){
			Product pProduct = (Product) pObject;
		
			setiKey(pProduct.getiKey());
			setcName(pProduct.getcName());
			setcDescription(pProduct.getcDescription());

			List<Image> lstImage = pProduct.getLstImages();
			List<ImageHttp> lstImageHttp = new ArrayList<ImageHttp>();
			for(Image img:lstImage){
				ImageHttp imageHttp = ImageHttp.getInstance();
				imageHttp.copy(img);
				lstImageHttp.add(imageHttp);
			}
			setLstImages(lstImageHttp);
			
			List<Product> lstProduct = pProduct.getLstProducts();
			List<ProductHttp> lstProductHttp = new ArrayList<ProductHttp>();
			for(Product prd:lstProduct){
				ProductHttp productHttp = ProductHttp.getInstance();
				productHttp.copy(prd);
				lstProductHttp.add(productHttp);
			}
			setLstProducts(lstProductHttp);
		} else {
			getL().info(getcIllegalArgumentException());
			throw new Exception(getcIllegalArgumentException());
		}
	}
	
	public JsonObject export(){
		JsonObject jSONObject = new JsonObject();
		
		if(getiKey() != 0)
			jSONObject.put("key", getiKey());
		jSONObject.put("name", getcName());
		jSONObject.put("description", getcDescription());
		
		//if(!(null == getParentProductHttp())){
		//	jSONObject.put("parentProduct", getParentProductHttp().export());
		//} else
			jSONObject.put("parentProduct", null);
		
		JsonArray jSONArrayImg = new JsonArray();
		if(getLstImages().size() > 0){
			for(ImageHttp img:getLstImages())
				jSONArrayImg.add(img.export());
			
			jSONObject.put("images", jSONArrayImg.toJson());	
		} else
			jSONObject.put("images", null);
		
		
		JsonArray jSONArrayProduct = new JsonArray();
		if(getLstProducts().size() > 0){
			for(ProductHttp prd:getLstProducts())
				jSONArrayProduct.add(prd.export());
			
			jSONObject.put("products", jSONArrayProduct.toJson());
		} else
			jSONObject.put("products", null);
		
		return jSONObject;
	}
	
	public Product exportProduct(){
		List<Image> lstImage = new ArrayList<Image>();
		List<Product> lstProduct = new ArrayList<Product>();
		
		Product prdExported = new Product(getiKey(), getcName(), getcDescription(), lstImage, lstProduct);
		
		for(ImageHttp img:getLstImages()){
			Image image = img.exportImage();
			image.setProduct(prdExported);
			lstImage.add(image);
		}
		
		for(ProductHttp prd:getLstProducts()){
			Product product = prd.exportProduct();
			product.setParentProduct(prdExported);
			lstProduct.add(product);
		}
		
		prdExported.setLstImages(lstImage);
		prdExported.setLstProducts(lstProduct);
		
		return prdExported;
	}

	public int getiKey() {
		return iKey;
	}

	public void setiKey(int iKey) {
		this.iKey = iKey;
	}

	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcDescription() {
		return cDescription;
	}

	public void setcDescription(String cDescription) {
		this.cDescription = cDescription;
	}

	public ProductHttp getParentProductHttp() {
		return parentProductHttp;
	}

	public void setParentProductHttp(ProductHttp parentProductHttp) {
		this.parentProductHttp = parentProductHttp;
	}

	public List<ImageHttp> getLstImages() {
		if(null == lstImages)
			setLstImages(new ArrayList<ImageHttp>());
		return lstImages;
	}

	public void setLstImages(List<ImageHttp> lstImages) {
		this.lstImages = lstImages;
	}

	private static ProductHttp get_uniqueInstance() {
		return _uniqueInstance;
	}

	private static void set_uniqueInstance(ProductHttp _uniqueInstance) {
		ProductHttp._uniqueInstance = _uniqueInstance;
	}

	public Logger getL() {
		return l;
	}

	public String getcIllegalArgumentException() {
		return cIllegalArgumentException;
	}

	public String getcIllegalAccessError() {
		return cIllegalAccessError;
	}

	public List<ProductHttp> getLstProducts() {
		if(null == lstProducts)
			setLstProducts(new ArrayList<ProductHttp>());
		return lstProducts;
	}

	public void setLstProducts(List<ProductHttp> lstProducts) {
		this.lstProducts = lstProducts;
	} 
}

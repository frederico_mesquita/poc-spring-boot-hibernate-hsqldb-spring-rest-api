package br.com.papodecafeteria.api.business;

import java.util.logging.Logger;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.papodecafeteria.api.dao.Product;
import br.com.papodecafeteria.api.dao.ProductDAO;
import br.com.papodecafeteria.api.http.ImageHttp;
import br.com.papodecafeteria.api.http.ProductHttp;

@Component
public class BProduct {
	
	protected static Logger l = Logger.getLogger("BProduct");
	
	@Autowired
	private ProductDAO productDAO;
	
	private ProductDAO getProductDAO(){
		return productDAO;
	}
	
	public JsonArray getLstProducts(){
		JsonArray jArray = new JsonArray();
		
		try{
			for(Product prd:getProductDAO().getProducts()){
				ProductHttp prdHttp = ProductHttp.getInstance();
				prdHttp.copy(prd);
				jArray.add(prdHttp.export());
			}
		} catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		return jArray;
	}
	
	public JsonArray getLstIsolatedProducts(){
		JsonArray jArray = new JsonArray();
		
		try{
			for(Product prd:getProductDAO().getProducts()){
				ProductHttp prdHttp = ProductHttp.getInstance();
				prdHttp.copy(prd);
				
				prdHttp.setLstProducts(null);
				prdHttp.setParentProductHttp(null);
				prdHttp.setLstImages(null);
				
				jArray.add(prdHttp.export());
			}
		} catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		return jArray;
	}
	
	public JsonObject getProduct(int pKey){
		ProductHttp prd = ProductHttp.getInstance(); 
		try {
			prd.copy(getProductDAO().getById(pKey));
		} catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		return prd.export();
	}
	
	public JsonObject getIsolatedProduct(int pKey){
		ProductHttp prd = ProductHttp.getInstance(); 
		try {
			prd.copy(getProductDAO().getById(pKey));
			
			prd.setLstImages(null);
			prd.setLstProducts(null);
			prd.setParentProductHttp(null);
			
		} catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		return prd.export();
	}

	public JsonObject postProduct(JsonObject pJSONObject){
		ProductHttp prd = ProductHttp.getInstance(pJSONObject);
		
		try {
			prd.copy(getProductDAO().save(prd.exportProduct()));
		} catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		
		return prd.export();
	}
	
	public JsonObject putProduct(JsonObject pJSONObject){
		ProductHttp prd = ProductHttp.getInstance(pJSONObject);

		getProductDAO().update(prd.exportProduct());
		
		return prd.export();
	}
	
	public JsonObject deleteProduct(int pKey){
		JsonObject jSONObject = getProduct(pKey);
		
		getProductDAO().deleteById(pKey);
		
		return jSONObject;
	}

	public JsonArray getLstProductsRelatatedByProductOrImage(int pKey){
		JsonArray jSONArray = new JsonArray();
		try{
			for(Product prd:getProductDAO().getLstProductsRelatatedByProductOrImage(pKey)){
				ProductHttp productHttp = ProductHttp.getInstance();
				productHttp.copy(prd);
				jSONArray.add(productHttp.export());
			}
		}catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		return jSONArray;
	}
	
	public JsonArray getIsolatedProductByIdRroductsRelatedByProductOrImage(int pKey, int pRelatedKey){
		JsonArray jSONArray = new JsonArray();
		try{
			for(Product prd:getProductDAO().getIsolatedProductByIdRroductsRelatedByProductOrImage(pKey, pRelatedKey)){
				ProductHttp productHttp = ProductHttp.getInstance();
				productHttp.copy(prd);
				
				productHttp.setLstImages(null);
				productHttp.setLstProducts(null);
				productHttp.setParentProductHttp(null);
				
				jSONArray.add(productHttp.export());
			}
		}catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		return jSONArray;
	}

	public JsonArray getChildrenProduct(int pKey){
		JsonArray jArray = new JsonArray();
		try {
			ProductHttp prd = ProductHttp.getInstance(); 
			prd.copy(getProductDAO().getById(pKey));
			
			for(ProductHttp prdHttp:prd.getLstProducts())
				jArray.add(prdHttp.export());
			
		} catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		return jArray;
	}

	public JsonArray getProductImages(int pKey){
		JsonArray jArray = new JsonArray();
		try {
			ProductHttp prd = ProductHttp.getInstance(); 
			prd.copy(getProductDAO().getById(pKey));
			
			for(ProductHttp prdHttp:prd.getLstProducts())
				for(ImageHttp img:prdHttp.getLstImages())
					jArray.add(img.export());
			
		} catch (Exception exc) {
			getL().info(exc.getMessage());
		}
		return jArray;
	}
	
	public static Logger getL() {
		return l;
	}
}

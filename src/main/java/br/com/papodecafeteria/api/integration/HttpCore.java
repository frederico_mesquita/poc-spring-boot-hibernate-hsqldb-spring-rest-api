package br.com.papodecafeteria.api.integration;

import org.json.simple.JsonObject;

public interface HttpCore {
	
	public void clear() throws Exception;
	
	public void reset() throws Exception;
	
	public JsonObject export();
	
	public void copy(Object pObject) throws Exception;

}

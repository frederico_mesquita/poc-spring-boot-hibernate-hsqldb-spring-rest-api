package br.com.papodecafeteria.api.resource;

import java.util.logging.Logger;

import org.json.simple.JsonArray;
import org.json.simple.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.papodecafeteria.api.business.BProduct;

import org.springframework.stereotype.Controller;

@Controller
public class ProductResource {
	protected Logger l = Logger.getLogger(getClass().getName());
	
	protected final String cIllegalArgumentException = "IllegalArgumentException";

	@Autowired
	private BProduct bProduct;
	
	private BProduct getBProduct(){
		return bProduct;
	}
	
	// ---------------- CRUD ---------------------------------------------------------------------------
	@RequestMapping(value = "/product", method = RequestMethod.POST)
	public ResponseEntity<JsonObject> postProduct(@RequestBody JsonObject pJSONObject){
		return new ResponseEntity<JsonObject>(getBProduct().postProduct(pJSONObject), HttpStatus.CREATED);
	}
	
	@RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
	public ResponseEntity<JsonObject> getProduct(@PathVariable("id") int pId){
		return new ResponseEntity<JsonObject>(getBProduct().getProduct(pId), HttpStatus.OK);
	}
	
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public ResponseEntity<JsonArray> getProducts() {
		return new ResponseEntity<JsonArray>(getBProduct().getLstProducts(), HttpStatus.OK);
	}

	@RequestMapping(value = "/product", method = RequestMethod.PUT)
	public ResponseEntity<JsonObject> putProduct(@RequestBody JsonObject pJSONObject){
		return new ResponseEntity<JsonObject>(getBProduct().putProduct(pJSONObject), HttpStatus.OK);
	}

	@RequestMapping(value = "/product/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<JsonObject> deleteProduct(@PathVariable("id") int pId){
		return new ResponseEntity<JsonObject>(getBProduct().deleteProduct(pId), HttpStatus.OK);
	}
	
	// Recupera todos os Produtos excluindo os relacionamentos 
	@RequestMapping(value = "/isolatedProducts", method = RequestMethod.GET)
		public ResponseEntity<JsonArray> getLstIsolatedProducts() {
		return new ResponseEntity<JsonArray>(getBProduct().getLstIsolatedProducts(), HttpStatus.OK);
	}

	// Recupera todos os Produtos incluindo um relacionamento específico (Produto ou Imagem)  
	@RequestMapping(value = "/productsRelatedByProductOrImage/{id}", method = RequestMethod.GET)
	public ResponseEntity<JsonArray> getLstProductsRelatatedByProductOrImage(@PathVariable("id") int pId) {
		return new ResponseEntity<JsonArray>(getBProduct().getLstProductsRelatatedByProductOrImage(pId), HttpStatus.OK);
	}
	
	// Recupera todos os Produtos (utilizando um id de produto específico) excluindo os relacionamentos
	@RequestMapping(value = "/isolatedProduct/{id}", method = RequestMethod.GET)
	public ResponseEntity<JsonObject> getIsolatedProduct(@PathVariable("id") int pId){
		return new ResponseEntity<JsonObject>(getBProduct().getIsolatedProduct(pId), HttpStatus.OK);
	}

	// Recupera todos os Produtos (utilizando um id de produto específico)excluíndo os relacionamentos
	//	eincluindo um relacionamento específico (Produto ou Imagem)  
	@RequestMapping(value = "/isolatedProducts/{id}/productsRelatedByProductOrImage/{relatedId}", method = RequestMethod.GET)
	public ResponseEntity<JsonArray> getIsolatedProductByIdRroductsRelatedByProductOrImage(@PathVariable("id") int pId, @PathVariable("relatedId") int pRelatedId) {
		return new ResponseEntity<JsonArray>(getBProduct().getIsolatedProductByIdRroductsRelatedByProductOrImage(pId, pRelatedId), HttpStatus.OK);
	}
	
	// Recupera a coleção de produtos filhos por um id de produto específico
	@RequestMapping(value = "/product/{id}/children", method = RequestMethod.GET)
	public ResponseEntity<JsonArray> getChildrenProduct(@PathVariable("id") int pId){
		return new ResponseEntity<JsonArray>(getBProduct().getChildrenProduct(pId), HttpStatus.OK);
	}
	
	// Recupera a coleção de Imagens para um id de produto específico
	@RequestMapping(value = "/product/{id}/images", method = RequestMethod.GET)
	public ResponseEntity<JsonArray> getProductImages(@PathVariable("id") int pId){
		return new ResponseEntity<JsonArray>(getBProduct().getProductImages(pId), HttpStatus.OK);
	}
	  
	@SuppressWarnings("unused")
	private Logger getL() {
		return l;
	}

	@SuppressWarnings("unused")
	private String getcIllegalArgumentException() {
		return cIllegalArgumentException;
	}
}

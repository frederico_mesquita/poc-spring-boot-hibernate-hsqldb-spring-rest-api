package br.com.papodecafeteria.api;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author 	Frederico Mesquita
 * @version	1.0-SNAPSHOT
 * @since	20/07/2017
 * 
 * 	Classe responsável pela execução da API.
 *
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
 
@SpringBootApplication
public class Main {
	private static final Logger l = Logger.getLogger(Main.class.getName());

	public static void main(String[] args) throws Exception {
		try{            
            SpringApplication.run(Main.class, args);
		} catch(Exception e){        
			l.log(Level.SEVERE, e.getMessage());                   
		}
	}
}
